# Mock Http Error

A lightweight http server that you can use to mock an error response:

```
$ python3 mock-http-error.py 505&

$ curl -I localhost:8000
HTTP/1.0 505 HTTP Version Not Supported
Server: BaseHTTP/0.6 Python/3.5.3
Date: Wed, 17 Oct 2018 07:07:50 GMT
Connection: close
Content-Type: text/html;charset=utf-8
Content-Length: 457

```
