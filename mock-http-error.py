#!/usr/bin/env python3

from http.server import *
import sys

def serve(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

def respond(code):
    # create classes on the fly!
    class Respond(BaseHTTPRequestHandler):
        pass
    Respond.do_GET = lambda self: self.send_error(code)
    Respond.do_HEAD = lambda self: self.send_error(code)
    return Respond

if __name__ == "__main__":
    expected_response = int(sys.argv[1])
    serve(handler_class=respond(expected_response))